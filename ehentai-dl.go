package main

import (
    "os"
    "fmt"
    "net/http"
    "io"
    "io/ioutil"
    "regexp"
    "strconv"
    "strings"
    "errors"
)

type behaviour int

const (
    ASK = iota
    OVERWRITE = iota
    SKIP_DIR = iota
    SKIP_IMG = iota
)

func check(err error) {
    if err != nil {
        panic(err)
    }
}

func tagsRx() *regexp.Regexp {
    return regexp.MustCompilePOSIX("Tags: ([^\"]*)\" />")
}

func infoRx() *regexp.Regexp {
    return regexp.MustCompilePOSIX("<h1 id=\"gn\">([^<>]+)</h1>.+<a href=\"https://e-hentai.org/uploader/[^\"]+\">([^<>]+)</a>&nbsp; <a href=\"[^\"]+\"><img class=\"ygm\" src=\"https://ehgt.org/g/ygm.png\" alt=\"PM\" title=\"Contact Uploader\" /></a></div><div id=\"gdd\"><table><tr><td class=\"gdt1\">Posted:</td><td class=\"gdt2\">([^<>]+)</td></tr><tr><td class=\"gdt1\">Parent:</td><td class=\"gdt2\">(<[^<>]+>)?([^<>]+)(<[^<>]+>)?</td></tr><tr><td class=\"gdt1\">Visible:</td><td class=\"gdt2\">([^<>]+)</td></tr><tr><td class=\"gdt1\">Language:</td><td class=\"gdt2\">([^> ]+) &nbsp;.*</td></tr><tr><td class=\"gdt1\">File Size:</td><td class=\"gdt2\">([^<>]+)</td></tr><tr><td class=\"gdt1\">Length:</td><td class=\"gdt2\">([^> ]+) pages</td></tr><tr><td class=\"gdt1\">Favorited:</td><td class=\"gdt2\" id=\"favcount\">([^> ]+) times</td></tr></table></div><div id=\"gdr\" onmouseout=\"[^\"]+\"><table><tr><td id=\"grt1\">Rating:</td><td id=\"grt2\"><div id=\"rating_image\" class=\"ir\" style=\"[^\"]+\"><img src=\"https://ehgt.org/g/blank.gif\" usemap=\"#rating\" /></div></td><td id=\"grt3\"><span id=\"rating_count\">([^<>]+)</span></td></tr><tr><td id=\"rating_label\" colspan=\"[0-9]\">Average: ([^ <]+)</td></tr></table>")
}

func imagePageUrlRx() *regexp.Regexp {
    return regexp.MustCompilePOSIX("<a href=\"(https://e-hentai.org/s/[^\"]*)\"><img alt=\"")
}

func imageUrlRx() *regexp.Regexp {
    return regexp.MustCompilePOSIX("<img id=\"img\" src=\"([^\"]*)\" style=\"[^\"]*\" onerror=\"[^\"]*\" />")
}

func securizeFileName(fileName string) string {
    var forbiddenChars = []rune{ '#', '%', '&', '{', '}', '(', ')', '[', ']', '\\', '<', '>', '*', '?', ' ', '/', '$', '!', '\'', '"', ':', '@', '+', '`', '|', '=', ';' }
    var res []rune
    for _, r := range fileName {
        ok := true
        for _, forbidden := range forbiddenChars {
            if r == forbidden {
                ok = false
                break
            }
        }
        if ok {
            res = append(res, r)
        } else {
            res = append(res, '_')
        }
    }
    return string(res)
}

func download(url string) []byte {
    req, err := http.NewRequest("GET", url, nil)
    check(err)
    req.AddCookie(&http.Cookie{Name: "nw", Value: "1"});
    res, err := (&http.Client{}).Do(req)
    check(err)
    body, err := ioutil.ReadAll(res.Body)
    check(err)
    return body
}

func downloadImage(url string, outputPath string) {
    file, err := os.Create(outputPath)
    check(err)
    res, err := http.Get(url)
    for err != nil {
        res, err = http.Get(url)
    }
    io.Copy(file, res.Body)
}

type Metadata struct {
    Url string
    Title string
    Tags string
    User string
    Date string
    Parent string
    Visible string
    Language string
    FileSize string
    NofPages int
    Favorited int
    NofRatings int
    Rating float64
    Images []string
}

func (metadata *Metadata) downloadIndexPage(i int) []byte {
    return download(fmt.Sprintf("%s?p=%d", metadata.Url, i))
}

func NewMetadata(url string, content []byte) Metadata {
    rawData := infoRx().FindSubmatch(content)
    if len(rawData) < 1 {
        panic(errors.New("metadata recovering failed for \"" + url + "\""))
    }
    rawData = rawData[1:]
    nofPages, err := strconv.Atoi(string(rawData[9]))
    check(err)
    favorited, err := strconv.Atoi(string(rawData[10]))
    check(err)
    nofRatings, err := strconv.Atoi(string(rawData[11]))
    check(err)
    rating, err := strconv.ParseFloat(string(rawData[12]), 64)
    check(err)
    parent := rawData[3]
    if (len(parent) == 0) {
        parent = rawData[4]
    } else {
        parent = regexp.MustCompilePOSIX("<a href=\"([^\"]+)\">").FindSubmatch(parent)[1]
    }
    if nofPages == 0 {
        panic(errors.New(fmt.Sprintf("error: parse of \"%s\" failed\n", url)))
    }
    return Metadata{
        Url: url,
        Title: string(rawData[0]),
        Tags: string(tagsRx().FindSubmatch(content)[1]),
        User: string(rawData[1]),
        Date: string(rawData[2]),
        Parent: string(parent),
        Visible: string(rawData[6]),
        Language: string(rawData[7]),
        FileSize: string(rawData[8]),
        NofPages: nofPages,
        Favorited: favorited,
        NofRatings: nofRatings,
        Rating: rating,
        Images: make([]string, nofPages),
    }
}

func (metadata *Metadata) nofIndexPages() int {
    return metadata.NofPages / 40 + 1
}

func (md Metadata) String() string {
    res := fmt.Sprintf("url: %s\ntitle: %s\ntags: %s\nuser: %s\ndate: %s\nparent: %s\nvisible: %s\nlanguage: %s\nfile_size: %s\npage_number: %d\nfavorited:  %d\nrating_number: %d\nrating: %f\n\nimages:",
        md.Url, md.Title, md.Tags, md.User, md.Date, md.Parent,
        md.Visible, md.Language, md.FileSize, md.NofPages,
        md.Favorited, md.NofRatings, md.Rating)
    for _, url := range md.Images {
        res += "\n"
        res += url
    }
    return res
}

func (metadata *Metadata) downloadAllIndexPages(firstPageContent []byte, c chan []byte) {
    nofIndexPages := metadata.nofIndexPages()
    c <- firstPageContent
    for i := 1; i < nofIndexPages; i++ {
        c <- metadata.downloadIndexPage(i)
    }
    close(c)
}

func sendPageUrls(indexPageContent []byte, pageUrlChnl chan string) {
    matches := imagePageUrlRx().FindAllSubmatch(indexPageContent, -1)
    for _, match := range matches {
        pageUrlChnl <- string(match[1])
    }
}

func printDownloadBar(i int, j int, n int) {
    fmt.Print("\r<[")
    for k := 0; k < n; k++ {
        if j == k {
            fmt.Print("]")
        }
        if i > k {
            fmt.Print("=")
        } else {
            fmt.Print(" ")
        }
    }
    if j == n {
        fmt.Print("]")
    }
    fmt.Print(">")
}

func (metadata *Metadata) downloadDisplayer(detected chan int, downloaded chan int) {
    fmt.Println(metadata.Title)
    detectedCounter := 0
    downloadedCounter := 0
    printDownloadBar(0, 0, metadata.NofPages)
    for detectedCounter < metadata.NofPages || downloadedCounter < metadata.NofPages {
        select {
        case <-detected:
            detectedCounter++
        case <-downloaded:
            downloadedCounter++
        }
        printDownloadBar(downloadedCounter, detectedCounter, metadata.NofPages)
    }
    close(detected)
    close(downloaded)
    fmt.Print("\r")
    for i := 0; i < (metadata.NofPages + 4); i++ {
        fmt.Print(" ")
    }
    fmt.Print("\r")
}

func askForOverwrite(dirName string) bool {
    var input string
    retry := true
    for retry {
        retry = false
        fmt.Printf("directory \"%s\" already exists, overwrite? [y/N] ", dirName);
        fmt.Scanf("%v", &input)
        input = strings.ToLower(input)
        switch input {
        case "y":
            return true
        case "n":
        case "":
        default:
            retry = true
        }
    }
    return false
}

func generateImagePath(i int, dirName string, url string) string {
    fileName := securizeFileName(regexp.MustCompilePOSIX("/([^/]+)$").FindStringSubmatch(url)[1])
    if fileName == "509.gif" {
        panic(errors.New("error: bandwidth exceeded"))
    }
    fileName = fmt.Sprintf("%04d_%s", i, fileName)
    return dirName + "/" + fileName
}

func fileNotExist(path string) bool {
    _, err := os.Stat(path)
    return os.IsNotExist(err)
}

func downloadDoujinshi(url string, b behaviour) int {
    res := 0
    content := download(url)
    metadata := NewMetadata(url, content)

    dtChnl := make(chan int)
    dlChnl := make(chan int)

    dirName := securizeFileName(metadata.Title)
    if b == OVERWRITE {
        os.Mkdir(dirName, 0755)
    } else {
        if fileNotExist(dirName) {
            os.Mkdir(dirName, 0755)
        } else {
            if b != SKIP_IMG {
                if b == SKIP_DIR || !askForOverwrite(dirName) {
                    return 0;
                }
            }
        }
    }

    indexPageChnl := make(chan []byte)
    pageUrlChnl := make(chan string)
    go metadata.downloadAllIndexPages(content, indexPageChnl)
    go func() {
        for indexPageContent := range indexPageChnl {
            sendPageUrls(indexPageContent, pageUrlChnl)
        }
        close(pageUrlChnl)
    }()
    go func() {
        i := 0
        for pageUrl := range pageUrlChnl {
            imageUrl := string(imageUrlRx().FindSubmatch(download(pageUrl))[1])
            metadata.Images[i] = imageUrl
            dtChnl <- i
            //go
            func(i int, imageUrl string, doneChnl chan int) {
                imagePath := generateImagePath(i, dirName, imageUrl)
                if b != SKIP_IMG || fileNotExist(imagePath) {
                    res += 1
                    downloadImage(imageUrl, imagePath)
                }
                dlChnl <- i
            }(i, imageUrl, dlChnl)
            i++
        }
    }()
    metadata.downloadDisplayer(dtChnl, dlChnl)
    err := ioutil.WriteFile(dirName + "/metadata.txt", []byte(metadata.String()), 0644)
    check(err)
    return res
}

func usage(executable string) {
    fmt.Printf("usage:\n    %s [ -n | -N | -f ] [ URL... ]\n        -n    skip image if existing\n        -N    skip directory if existing\n        -f    overwrite if existing\n", executable)
}

func main() {
    args := os.Args[1:]
    if len(args) == 0 {
        usage(os.Args[0])
    } else {
        var b behaviour = ASK
        for _, arg := range args {
            switch arg {
            case "-f":
                b = OVERWRITE
            case "-N":
                b = SKIP_DIR
            case "-n":
                b = SKIP_IMG
            default:
            }
        }
        i := 0
        for _, url := range args {
            switch url {
            case "-f":
            case "-n":
            case "-N":
            default:
                i += downloadDoujinshi(url, b)
            }
        }
        fmt.Println("downloaded images:", i)
    }
}
